#ifndef SPRITESHEETTESTER_H
#define SPRITESHEETTESTER_H

#include "updatablesprite.h"

class SpritesheetTester : public UpdatableSprite
{
	public:
		SpritesheetTester(std::string spritesheetName);
		
		void update();
		
		virtual void draw(sf::RenderWindow* window);
};

#endif
