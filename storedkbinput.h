#ifndef STOREDKBINPUT_H
#define STOREDKBINPUT_H


#include <map>
#include "SFML/Window.hpp"

typedef std::map<sf::Keyboard::Key,bool> StoredKBInput;

#endif // !STOREDKBINPUT_H
