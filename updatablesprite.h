#ifndef UPDATABLESPRITE_H
#define UPDATABLESPRITE_H

#include "resourceuser.h"

class UpdatableSprite : public ResourceUser
{
	public:
		UpdatableSprite();
		
		UpdatableSprite(std::string spritesheetName);
		
		virtual void update();
	protected:
};

#endif
