#ifndef MENUSCREEN_H
#define MENUSCREEN_H

#include "button.h"
#include <string>


class MenuScreen {
public:
	
	
	
	MenuScreen();
	
	void linkMenu(std::string name, MenuScreen* newLink);
	
	void addButton(Button newButton);
	
private:
	std::map<std::string, MenuScreen*> linkedMenus;
	std::vector<Button> buttons;
	
};

#endif
