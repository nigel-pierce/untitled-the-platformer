#include "menu.h"

Menu::Menu() {
	m_window = NULL;
}

Menu::Menu(sf::RenderWindow *window)
{
	std::cout << "Constructing a Menu!\nSuccessful adding sheet playButton? " 
		<< std::endl;
  m_window = window;
  std::cout << ResourceUser::addSpritesheet("playButton", "Play all 3.png") 
		<< std::endl;
  menuItems["play"] = new Button("playButton", sf::IntRect(0, 0, 118, 43), 
		sf::IntRect(119, 0, 118, 43), sf::IntRect(240, 0, 118, 43));
  Button *playButton = menuItems["play"];
  playButton->setMeaning("play");
  playButton->setPosition(200, 200);
}

void Menu::init()
{
}

//void Menu::AddMenuItem(MenuItem *newItem)
//{
//	_menuItems[newItem->GetId()] = /*&*/newItem;
//}

MenuResult Menu::doMenu()
{
	std::cout << "In Menu::doMenu()" << std::endl;
  m_window->clear(sf::Color(0,255,0));

  std::map<std::string, Button*>::iterator menuItem = menuItems.begin();
  for (menuItem; menuItem != menuItems.end(); menuItem++)
  {
  	std::cout << "Drawing menuItem[\"" << menuItem->first << "\"]" << std::endl;
    menuItem->second->draw(m_window);
  }
  

  m_window->display();

  return getMenuResponse();
}

//MenuItem& MainMenu::GetMenuItem(std::string id)
//{
//    return *_menuItems[id];
//}

MenuResult Menu::getMenuResponse()
{
  sf::Event event;
  while(m_window->pollEvent(event))
  {
    if (event.type == sf::Event::MouseButtonPressed 
			|| event.type == sf::Event::MouseButtonReleased)
    {
    	int bob = event.mouseButton.x; // thanks Bob
      return handleClick(event);
    }
    
    if (event.type == sf::Event::Closed)
    {
      return Exit;
    }
  }
}

MenuResult Menu::handleClick(sf::Event mouseEvent)
{
  /*std::map<std::string,MenuItem*>::iterator it;
  for (it = _menuItems.begin(); it != _menuItems.end(); it++)
  {
    sf::Rect<int> menuItemRect = (*it).second->GetBoundary();
    if (menuItemRect.Bottom > y && menuItemRect.Top < y &&
        menuItemRect.Left < x && menuItemRect.Right > x)
    {
      it->second->Clicked();
	  if (it->second->GetType() == "MenuButton")
	  {
		  MenuButton *theButton = dynamic_cast<MenuButton*>(it->second);
		  //static_cast<MenuButton>(it->second);
		  MenuResult theAction = theButton->GetAction();
		return theAction;
	  }
    }
  }
  return Nothing;*/
  
  std::map<std::string, Button*>::iterator it;
  for (it = menuItems.begin(); it != menuItems.end(); it++)
  {
  	Button *item = it->second;
  	item->update(mouseEvent);
  	if (item->isClicked())
  	{
  		// stubbed for now
  		return Play;
  	}
  }
  
  return Nothing;
}
