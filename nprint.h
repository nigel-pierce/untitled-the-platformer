#ifndef NPRINT_H
#define NPRINT_H

#include <string>
#include <ostream>

void nprint(std::string message);
void nprint(std::ostream& message);

#endif
