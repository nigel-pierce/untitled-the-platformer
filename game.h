#ifndef GAME_H
#define GAME_H

#include <set>
#include "sfml/Graphics.hpp"
#include "storedkbinput.h"
#include "nprint.h"
#include "resourceuser.h"
#include "spritesheettester.h"
#include "menu.h"

class Game
{
public:
	// Constructors
	Game();
	
	// typedefs and friends
	enum GameState {
		Uninitialized,
		ShowingSplash,
		Paused,
		ShowingMenu,
		ShowingInstructions,
		Playing,
		LoadingGame,
		GameOver,
		Exiting
	};
	
	typedef std::set<sf::Keyboard::Key> KeyCodes;
	

	// methods
	
	void start(); // First method called from main(); calls initialization methods 
		// and passes off execution to GameLoop() (or at least it's supposed to)
	
	// accessors
	
	sf::RenderWindow& GetWindow();
	
	// mutators
	void SetGameState(GameState newState);
	
	// utility
//	const StoredKBInput GetInput();

private:
	
	GameState m_gameState;
	sf::RenderWindow m_mainWindow;

	Menu m_menuMain;

	int m_framesPerSec;
	
	std::vector<ResourceUser> spriteUsers;
	
	// do stuff
	void InitGame();
	void GameLoop();
	void playGame();
	
	// Utility methods
	
	bool IsExiting();
};

#endif
