#include "game.h"
#include <iostream>

Game::Game()
{
	m_framesPerSec = 30;
	m_gameState = Uninitialized;
	m_menuMain = Menu(&m_mainWindow);
	nprint("Getting through constructor fine");
}

void Game::start()
{
	if(m_gameState != Uninitialized)
	{
    	return;
	}
	
	std::cout << "About to call InitGame()" << std::endl;
	
	InitGame();
  
  std::cout << "Game successfully initilized; about to start GameLoop()" << 
	 std::endl;
  
	GameLoop();
	
	
	m_mainWindow.close();
}

sf::RenderWindow& Game::GetWindow()
{
	return m_mainWindow;
}

void Game::SetGameState(GameState newState)
{
	std::cout << "Setting m_gameState to: " << newState << std::endl;
		//m_prevCycleState = m_gameState;
	m_gameState = newState;
	std::cout << "m_gameState has been set." << std::endl;
}
//
//
//const StoredKBInput Game::GetInput()
//{
//	StoredKBInput stored;
//
//	const sf::Input& inNow = m_mainWindow.GetInput();
//	for (KeyCodes::iterator itrKey=m_keysToCheck.begin(); 
//		itrKey!=m_keysToCheck.end(); itrKey++)
//	{
//		sf::Key::Code storo = *itrKey;
//		stored[storo] = inNow.IsKeyDown(storo);
//	}
//
//	return stored;
//}

void Game::InitGame()
{
	m_mainWindow.create(sf::VideoMode(640, 480, 32), "Big Bug Boss Battle!");
	
	std::cout << "About to create the Smiley ResourceUser :)" << std::endl;
	ResourceUser smileyGuy = ResourceUser();
	std::cout << "Back in InitGame() after creating the Smiley :)" << std::endl;
	std::cout << "About to push the smiley into spriteUsers" << std::endl;
	spriteUsers.push_back(smileyGuy); // this should cause the smiley to be 
		// created :_) :)
	std::cout << "Successfully pushed the smiley :)" << std::endl;
	
	// create spritesheettester to test stuff
	spriteUsers.push_back(SpritesheetTester("test-o-matic"));
	
	SetGameState(ShowingMenu);
	std::cout << "Indeed the game state has been set" << std::endl;
}

void Game::GameLoop()
{
	// replace the clock stuff with SFML's own automagic timing stuff
	sf::Clock timeguy;
	int willGetBig = 0;
	while(!IsExiting())
	{
		timeguy.restart();
		std::cout << "Number: " << willGetBig << std::endl;
		
		//m_inputThisCycle = GetInput();
	
		switch(m_gameState)
		{
			case Game::Playing:
				playGame();
	
				break;
			case LoadingGame:
				SetGameState(Playing);
				break;
			case ShowingMenu:
			{
				MenuResult result = m_menuMain.doMenu();
				if (result == Play)
				{
					SetGameState(Playing);
				}
				break;
			}
			case Game::GameOver:
				break;
			default:
				std::cout << "Game state is " << m_gameState << "; taking to action" << 
					std::endl;
		}
		
		float frameLength = timeguy.getElapsedTime().asSeconds();
		float timeLeftInFrame = (1.000 / m_framesPerSec) - frameLength;
		if (timeLeftInFrame > 0)
		{
			sf::sleep(sf::seconds(timeLeftInFrame));
		}
		willGetBig++;
	}
}

void Game::playGame()
{
	m_mainWindow.clear(sf::Color(150,0,200));

	// Draw all the stuff
	for (int i=0; i < spriteUsers.size(); i++)
	{
		std::cout << "Should be drawing spriteUser[" << i << "] now\n";
		spriteUsers[i].draw(&m_mainWindow);
	}
	m_mainWindow.display();
	
	std::cout << "Number of spritesheets: " << ResourceUser::numSpritesheets() 
	 << "\n";

	// change game state depending on window events
	sf::Event currentEvent;
	m_mainWindow.pollEvent(currentEvent); // this kills the feel (but not anymore)
	if(currentEvent.type == sf::Event::Closed)
	{
		SetGameState(Game::Exiting);
	}
	if(currentEvent.type == sf::Event::KeyPressed)
	{
		if(sf::Keyboard::Escape == currentEvent.key.code)
		{
			//ShowMenu();
		}
	}
}

bool Game::IsExiting()
{
	return m_gameState == Game::Exiting;
}
