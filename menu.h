#ifndef MAIN_MENU_H
#define MAIN_MENU_H

#include <list>
#include <iostream>
#include "SFML/Graphics.hpp"
//#include "menuitem.h"
//#include "menubutton.h"
#include "button.h"

// this would be in menuitem.h but I haven't brought that in yet
enum MenuResult {
	Nothing,
	Exit,
	Play,
	SettingsChange,
	Instructions,
	GotoMainMenu
};

class Menu
{
public:
	Menu();
	Menu(sf::RenderWindow *window);

	void init();
	//void addMenuItem(MenuItem *newItem);

	// PUBLIC METHODS

	MenuResult doMenu();

	//MenuItem& getMenuItem(std::string id);

private:
	
	// MEMBER VARIABLES
	
	sf::RenderWindow *m_window;
	
	MenuResult getMenuResponse();
	MenuResult handleClick(sf::Event mouseEvent);
	MenuResult handleMouseLeftDown(int x, int y);
	//std::list<MenuItem> _menuItems; Let's make this a map
	//std::map<std::string, MenuItem*> m_menuItems;
	// for now it's just buttons :P
	std::map<std::string, Button*> menuItems;
};

#endif
