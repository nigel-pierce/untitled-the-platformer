#include "button.h"

// CONSTRUCTORS

Button::Button() : UpdatableSprite()
{
	meaning = "";
	clicked = false;
	mode = BUTTONNEUTRAL;
	
	// remove the old smiley
	spriteFrames.clear();
	
	// add the smiley button
	addSprite("smiley", sf::IntRect(0, 0, 32, 32));
	addSprite("smiley", sf::IntRect(0, 0, 16, 32));
	addSprite("smiley", sf::IntRect(0, 0, 24, 32));
}

Button::Button(std::string spritesheetName, sf::Rect<int> rectNeutral, sf::Rect<int> rectHover, 
	sf::Rect<int> rectActive) : UpdatableSprite(spritesheetName)
{
 	meaning = "";
 	clicked = false;
	mode = BUTTONNEUTRAL;
	
 	if (spriteFrames.size() > 0)
 	{
 		/*for (int i=0; i < spriteFrames.size(); i++)
 		{
 			spriteFrames.erase(*/
 		spriteFrames.clear();
 	}
	addSprite(spritesheetName, rectNeutral);
	//spriteFrames[0].x;
	// make it also have upper left corner and stuff
	addSprite(spritesheetName, rectHover);
	addSprite(spritesheetName, rectActive);
}

// ACCESSORS

void Button::setMeaning(std::string newMeaning)
{
	meaning = newMeaning;
}

std::string Button::getMeaning()
{
/**/ // :)
	return meaning;
}

bool Button::isClicked()
{
	return clicked;
}

// PUBLIC METHODS

void Button::update(int mouseX, int mouseY)
{
	sf::Rect<int> currentArea = getCurrentArea(); // depends on mode
	if (currentArea.contains(mouseX, mouseY))
	{
		clicked = true;
	}
}

void Button::update(sf::Event event)
{
	sf::Rect<int> currentArea = getCurrentArea(); // depends on mode
	if (event.type == sf::Event::MouseButtonPressed)
	{
		if (currentArea.contains(event.mouseButton.x, event.mouseButton.y))
		{	
			mode = BUTTONACTIVE;
		}
		else
		{
			mode = BUTTONNEUTRAL;
		}
	}
	else if (event.type==sf::Event::MouseButtonReleased)
	{ 
		if (currentArea.contains(event.mouseButton.x, event.mouseButton.y))
		{
			if (mode==BUTTONACTIVE)
			{
				mode = BUTTONCLICKED;
				clicked = true;
			}
		}	
		else
		{
			mode = BUTTONNEUTRAL;
		}
	}	
}	

void Button::draw(sf::RenderWindow* window)
{
	std::cout << "Current button mode: " << mode << std::endl;
	window->draw(spriteFrames[mode]);
}

// PRIVATE METHODS

sf::Rect<int> Button::getCurrentArea()
{
	sf::Rect<int> current = getCurrentSpriteRect();
	return sf::Rect<int>(position.x, position.y, current.width, current.height);
}

sf::Rect<int> Button::getCurrentSpriteRect()
{
	// yeah I know I could just go "return spriteFrames[mode].getTextureRect();" but that would be 
	// too cheesey. I guess.
	if (mode == BUTTONNEUTRAL)
	{
		return spriteFrames[0].getTextureRect();
	}
	else if (mode == BUTTONHOVER)
	{
		return spriteFrames[1].getTextureRect();
	}
	else if (mode == BUTTONACTIVE)
	{
		return spriteFrames[2].getTextureRect();
	}
}
