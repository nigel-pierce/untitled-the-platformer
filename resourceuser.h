#ifndef RESOURCEUSER_H
#define RESOURCEUSER_H

#include <map>
#include <SFML/Graphics.hpp>

// This will be the parent class for every game object that uses some resource, 
// such as a spritesheet, sound, or something else.
// To start with it will just store spritesheets.

class ResourceUser
{
	public:
		// Constructors
		
		ResourceUser(); // default b/c I don't even know what this class wants yet
		ResourceUser(std::string spritesheetName);
		
		// Typedefs
		
		typedef std::map<std::string, sf::Texture> Sheets; // string part will 
			// generally be filename w/o extension
		typedef std::map<std::string, ResourceUser> ResUsers; // string will be 
			// unique name/id (some will be programmer-defined; otherwise auto-
			// generated)
		
		// ACCESSORS
		
		void setPosition(int x, int y);
		void setPosition(sf::Vector2<int> newPosition);
		
		// OTHER METHODS
		
		void addSprite(std::string sheetName, sf::IntRect area); // Adds sprite to 
			// spriteFrames, referencing specified sheet (loading it if it isn't 
			// already) and specified rectangle in sheet.
		void removeSprite(int index); // does nothing if invalid index
		void clearSprites(); // does what you think it does
		
		static bool addSpritesheet(std::string name, std::string fileName); // loads from resources directory
		
		virtual void draw(sf::RenderWindow* window); // Draws individual 
			// ResourceUser at current frame
		
		static int numSpritesheets();
		
	protected:
		
		// MEMBER VARIABLES
		
		sf::Vector2<int> position;
		
		// Static resources; static b/c likely shared
		static Sheets spritesheets;
		// more vectors of other types of resources (sounds etc.)
		
		// also a container for all the ResourceUsers (Crossing my fingers Xo!)
		static ResUsers resourceUsers;
		
		static void addDefaultTexture(); // adds the default smiley =)
		
		// non-static; per-object
		
		// constructor hemper
		void m_init(); // called by all constructors to make default stuff
		
		std::vector<sf::Sprite> spriteFrames;
			// perhaps I should make another SpriteFrame class, which stores the 
			// pointer to the appropriate spritesheet and the rect that defines the 
			// frame. I guess it could just be a struct tho.
			// btw I'm just using bools as placeholders alot here. lol "alot"
			// You know it would probably be nice if I could just have one vector 
			// that stores references to every resource the object uses, irrespective 
			// of type. But that would be too much work to set up.
		// more vectors of other types of resources (sounds etc.)
		
		int currentFrame;
		
		// methods
		
		static void removeResource(std::string resourceName); // ONLY use when sure 
			// that no-one else will want to use this resource soon (e.g. at end of 
			// level or you just defeated all enemies who share a spritesheet and new 
			// ones won't be spawning soon)
			
		static void loadResource(std::string resourceName); // Checks if named 
			// resource is already loaded, and loads it if it isn't. resourceName 
			// also can be a "sub-resource" or something, where it names something 
			// that actually lives in a larger resource (like the villain's sprite 
			// area in a larger "baddies" spritesheet).
			
		static bool isResourceLoaded(std::string resourceName);
		
		//static void addUser
			
		// non-static methods
		
};

#endif
