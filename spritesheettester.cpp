#include "spritesheettester.h"

SpritesheetTester::SpritesheetTester(std::string spritesheetName) 
: UpdatableSprite(spritesheetName)
{
	// going to make the frames be 32x32 and stuff
	int frameDim = 32;
	
	sf::Texture* sheet = &spritesheets[spritesheetName];
	
	int x = 0;
	int y = 0;
	while (y < sheet->getSize().y)
	{
		while (x < sheet->getSize().x)
		{
			addSprite(spritesheetName, sf::IntRect(x, y, frameDim, frameDim));
			currentFrame = 0;
		}
	}
}

void SpritesheetTester::update() {
	if (currentFrame >= spriteFrames.size())
	{
		currentFrame = 0;
	}
	else if (currentFrame == -1 && spriteFrames.size() > 0)
	{
		currentFrame = 0;
	}
	else
	{
		currentFrame++;
	}
}

void SpritesheetTester::draw(sf::RenderWindow* window)
{
	if (currentFrame >= 0 && currentFrame < spriteFrames.size())
	{
		window->draw(spriteFrames[currentFrame]);
	}
	else
	{
		window->draw(sf::Sprite(spritesheets["smiley"]));
	}
}
