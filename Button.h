#ifndef BUTTON_H
#define BUTTON_H

#include <string>
#include <iostream>
#include "sfml/Graphics.hpp"
#include "updatablesprite.h"

class Button : public UpdatableSprite {
public:
	
	// CONSTRUCTORS
	Button();
	
	Button(std::string spritesheetName, sf::Rect<int> rectNeutral, 
	 sf::Rect<int> rectHover, sf::Rect<int> rectActive);
	
	// ACCESSORS
	
	void setMeaning(std::string newMeaning);
	std::string getMeaning();
	
	bool isClicked();
	
	// METHODS
	
	void update(int mouseX, int mouseY);
	void update(sf::Event event);
	
	void draw(sf::RenderWindow* window);
	
protected:
	
	// MEMBER VARIABLES
	
	enum ButtonMode {
		BUTTONNEUTRAL = 0,
		BUTTONHOVER = 1,
		BUTTONACTIVE = 2,
		BUTTONCLICKED
	} mode;
	
	std::string meaning;
	
	bool clicked;
	
	// MEMBER FUNCTIONS
	
	sf::Rect<int> getCurrentArea();
	sf::Rect<int> getCurrentSpriteRect();
	
};

#endif
