#include "nprint.h"
#include <iostream>

void nprint(std::string message)
{
	std::cout << message << std::endl;
}

void nprint(std::ostream& message)
{
	std::cout << message << std::endl;
}
